# Embedded

This repository contains the code source of the firmware of the Monomackie device.

Monomackie is a system that composed of both a physical device and a suite of integration on computer. The purpose of Monomackie is to make the management of the sound on a computer easy and fluent.

This firmware is suit for the Arduino Nano card, wired as describe by the team.

## Compile and engrave

Please use Platformio.

With Platformio/Clion, you can follow this tutorial : <https://docs.platformio.org/en/latest/integration/ide/clion.html> .

When you clone, notice that you will have to re-init the projet from the Platformio integration, change the Cmake profile and then create a configurations "Upload".
