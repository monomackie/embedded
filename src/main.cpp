#include <Arduino.h>

#include "Master.h"
#include "Slave.h"
#include "Commons.h"

bool isMaster = false;

void setup() {
    Commons::setupHW();
    isMaster = Commons::setupOperations();

    if (isMaster) {
        Master::setup();
    }
    else {
        Slave::setup();
    }
}

void loop() {
    if (isMaster) {
        Master::loop();
    }
    else {
        Slave::loop();
    }
}