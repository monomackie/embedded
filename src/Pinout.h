#pragma once

#ifdef SEEED_XIAO_M0 //building for XIAO board

#define POTENTIOMETER_PIN 6
#define LEFT_BUTTON_PIN 7
#define RIGHT_BUTTON_PIN 9

#define TFT_RST_PIN 1
#define TFT_DC_PIN  0
#define TFT_CS_PIN  -1

// TFT MOSI =  (hardware defined)
// TFT SCK =  (hardware defined)

#define MSTING_OUTPUT_PIN 2
#define MSTING_INPUT_PIN 3
// MODULARITY SDA = 0 (hardware defined)
// MODULARITY SCK = 0 (hardware defined)

#else //Default Arduino Nano board

#define POTENTIOMETER_PIN A0
#define LEFT_BUTTON_PIN 4
#define RIGHT_BUTTON_PIN 2

#define TFT_CS_PIN  10
#define TFT_DC_PIN  8
#define TFT_RST_PIN 9

// TFT MOSI = 11 (hardware defined)
// TFT SCK = 13 (hardware defined)

#define MSTING_OUTPUT_PIN 5
#define MSTING_INPUT_PIN 6
// MODULARITY SDA = A4 (hardware defined)
// MODULARITY SCK = A5 (hardware defined)

#endif
