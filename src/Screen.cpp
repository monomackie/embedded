#include "Screen.h"
#include "Pinout.h"

#define WIDTH 128
#define HEIGHT 128

#define SCREEN_X_OFFSET 3
#define SCREEN_Y_OFFSET 3
#define SCREEN_LINE_HEIGHT 21

Screen::Screen() {
	m_Tft = new Adafruit_ST7735(TFT_CS_PIN, TFT_DC_PIN, TFT_RST_PIN);
	m_Tft->initR(INITR_144GREENTAB);
	m_Tft->fillScreen(ST77XX_BLACK);
	for (uint8_t line = 0 ; line < SCREEN_MAX_LINE_NUMBER ; line ++) {
		m_MustRefresh[line] = false;
		m_textColor[line].r = 255;
		m_textColor[line].g = 255;
		m_textColor[line].b = 255;
		m_backgroundColor[line].r = 255;
		m_backgroundColor[line].g = 255;
		m_backgroundColor[line].b = 255;
		m_FontSize[line] = 1;
		m_TextWrap[line] = false;
	}

    for (unsigned char & i : m_Text) {
        i = 0;
    }
}

void Screen::setTextDisplay(uint8_t *text, uint8_t line, uint8_t fontSize, uint8_t text_r, uint8_t text_g, uint8_t text_b, uint8_t bg_r, uint8_t bg_g, uint8_t bg_b, bool wrap) {
    setText(text, line);
	setTextColor(text_r, text_g, text_b, line);
	setBackgroundColor(bg_r, bg_g, bg_b, line);
    setFontSize(fontSize, line);
	setTextWrap(wrap, line);
}

void Screen::setText(const uint8_t *text, uint8_t line) {
	for (uint8_t i = 0 ; i < SCREEN_MAX_CHAR_PER_LINE ; i++) {
		if (i < SCREEN_MAX_TEXT_SIZE) {
			m_Text[i+ line * SCREEN_MAX_CHAR_PER_LINE] = text[i];
		}
	}
	m_Text[(line+1) * SCREEN_MAX_CHAR_PER_LINE - 1] = '\0';

	m_MustRefresh[line] = true;
}

void Screen::setFontSize(uint8_t size, uint8_t line) {
    if (size != m_FontSize[line]) {
        m_FontSize[line] = size;
        m_MustRefresh[line] = true;
    }
}
void Screen::setTextColor(uint8_t r, uint8_t g, uint8_t b, uint8_t line) {
    if (m_textColor[line].r != r || m_textColor[line].g != g || m_textColor[line].b != b) {
		m_textColor[line].r = r;
		m_textColor[line].g = g;
		m_textColor[line].b = b;
        m_MustRefresh[line] = true;
    }
}

void Screen::setBackgroundColor(uint8_t r, uint8_t g, uint8_t b, uint8_t line) {
	if (m_backgroundColor[line].r != r || m_backgroundColor[line].g != g || m_backgroundColor[line].b != b) {
		m_backgroundColor[line].r = r;
		m_backgroundColor[line].g = g;
		m_backgroundColor[line].b = b;
		m_MustRefresh[line] = true;
	}
}

void Screen::drawText() {
	//clearScreen(); left it until we make sure the display works great without it
	for(int line = 0 ; line < SCREEN_MAX_LINE_NUMBER ; line++) {
    	if (m_MustRefresh[line]) {
            m_Tft->setCursor(SCREEN_X_OFFSET, line * SCREEN_LINE_HEIGHT + SCREEN_Y_OFFSET);
            m_Tft->setTextColor(RGBto565(m_textColor[line]), RGBto565(m_backgroundColor[line]));
            m_Tft->setTextSize(m_FontSize[line]);
            m_Tft->setTextWrap(m_TextWrap[line]);
            m_Tft->print((char *) m_Text + line * SCREEN_MAX_CHAR_PER_LINE);
        }
        m_MustRefresh[line] = false;
    }
}

void Screen::clearScreen() {
	m_Tft->fillScreen(ST77XX_BLACK);
}

uint16_t Screen::RGBto565(struct SCREEN_Color color) {
	int r = (int)((((float) color.r)/255.f) * 31);
	int g = (int)((((float) color.g)/255.f) * 63);
	int b = (int)((((float) color.b)/255.f) * 31);
	return r<<11 | g<<5 | b;
}

void Screen::setTextWrap(bool wrap, uint8_t line) {
	m_TextWrap[line] = wrap;
}
