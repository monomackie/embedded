#ifndef SCREEN_H
#define SCREEN_H 

#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>

#define SCREEN_MAX_TEXT_SIZE    50

#define SCREEN_MAX_CHAR_PER_LINE 12 //10 fully displayed, 1 half cutted, 1 end of string '\0'
#define SCREEN_MAX_LINE_NUMBER 10

struct SCREEN_Color {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

class Screen {
private:
	Adafruit_ST7735 	*m_Tft;
    bool                m_MustRefresh[SCREEN_MAX_LINE_NUMBER];
	bool 				m_TextWrap[SCREEN_MAX_LINE_NUMBER];
    uint8_t             m_FontSize[SCREEN_MAX_LINE_NUMBER];
    struct SCREEN_Color m_textColor[SCREEN_MAX_LINE_NUMBER], m_backgroundColor[SCREEN_MAX_LINE_NUMBER];
    uint8_t             m_Text[SCREEN_MAX_CHAR_PER_LINE * SCREEN_MAX_LINE_NUMBER];
public:
  Screen();
  void clearScreen();
  void setText(const uint8_t *text, uint8_t line);
  void setFontSize(uint8_t size, uint8_t line);
  void setTextColor(uint8_t r, uint8_t g, uint8_t b, uint8_t line);
  void setBackgroundColor(uint8_t r, uint8_t g, uint8_t b, uint8_t line);
  void setTextWrap(bool wrap, uint8_t line);
  void drawText();
  static uint16_t RGBto565(SCREEN_Color color);

  void setTextDisplay(uint8_t *text, uint8_t line, uint8_t fontSize,
					  uint8_t text_r, uint8_t text_g, uint8_t text_b,
					  uint8_t bg_r, uint8_t bg_g, uint8_t bg_b,
					  bool wrap);
};

#endif
