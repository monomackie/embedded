#ifndef EMBEDDED_MASTER_H
#define EMBEDDED_MASTER_H

#include "Screen.h"

#define HEARTBEAT_RECEPTION_PERIOD_MS 2200
#define HEARTBEAT_EMISSION_PERIOD_MS 1000

class Master {
private:
    static void searchForNewcomers();
    static void querySlavesStatus();
public:
    static void setup();
    static void loop();
};

#endif //EMBEDDED_MASTER_H
