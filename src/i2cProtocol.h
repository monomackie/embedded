#ifndef I2C_PROTOCOL_H
#define I2C_PROTOCOL_H
#define I2C_PROTOCOL_VERSION 2

#define I2C_WAIT_DELAY            50
#define I2C_DEFAULT_ID            127

#define I2C_IDASSIGNATION_FUNCCODE      1
#define I2C_IDASSIGNATION_SIZE          2

#define I2C_IDACKNOWLEDGE_FUNCCODE      1
#define I2C_IDACKNOWLEDGE_SIZE          1

#define I2C_REQUEST_STATUS_FUNCCODE     2
#define I2C_REQUEST_STATUS_SIZE         1

#define I2C_SEND_STATUS_FUNCCODE        2
#define I2C_SEND_STATUS_SIZE            4

#define I2C_SESSION_NAME_FUNCCODE       3
#define I2C_SESSION_NAME_SIZE           26

#endif
