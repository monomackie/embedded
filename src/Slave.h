#ifndef EMBEDDED_SLAVE_H
#define EMBEDDED_SLAVE_H

class Slave {
private:
    static void requestI2CEvent();
    static void receiveI2CEvent(int howMany);
    static void updateI2C();
public:
    static void setup();
    static void loop();

    static void updateDisplayRole();
};

#endif //EMBEDDED_SLAVE_H
