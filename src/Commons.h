#ifndef EMBEDDED_COMMONS_H
#define EMBEDDED_COMMONS_H

#include "Screen.h"

#define CONNECTION_STATE_DISCONNECTED  	0
#define CONNECTION_STATE_CONNECTED    	1
#define BUTTON_COUNT    				2

#define MAX_SLAVES  10
#define MAX_DEVICES (MAX_SLAVES+1)

#define DISPLAY_ROLE_LINE 0
#define DISPLAY_USB_LINE 1
#define DISPLAY_SESSION_NAME_LINE 2
#define DISPLAY_MODULARITY_LINE 3

struct Button {
    uint8_t pin;
    bool pressed;
};

struct Timer {
    unsigned long old_time;
    unsigned long new_time;
};
struct DeviceStatus {
    uint16_t  potentiometerValue;
    uint8_t   buttonMask; //0b876543210, 1 = pushed, 0 = not pushed, refer to buttons in the main file for the bits assignation
};

class Commons {
public:
	static Screen*  screen;
	static uint8_t  i2cID;
	static bool     devicePotentiometerHasChanged[MAX_DEVICES];
	static volatile struct DeviceStatus thisDeviceStatus;
	static struct  	DeviceStatus devicesStatus[MAX_DEVICES];

    static void 	setupHW();
    static bool 	setupOperations();
    static bool 	hasTimeElapsed(Timer *timer, unsigned long duration);
    static void 	sendSting();
    static void 	updateInputs();
	static uint8_t 	convert1024To100(int value);
	static uint16_t roundUp1024(int value);

};

#endif //EMBEDDED_COMMONS_H
