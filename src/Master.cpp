#include "Master.h"
#include <Wire.h>
#include <serialProtocol.h>
#include "i2cProtocol.h"
#include "Commons.h"

uint8_t I2cLastUnusedID;
int     ConnectionState;
struct  Timer ping_timer = {0, 0}, id_assignment_timer = {0, 0}, slave_status_query_timer = {0, 0};
unsigned long	heartBeatReceptionDate;
unsigned long 	heartBeatEmissionDate;	//emergency heartbeat
bool			heartBeatReceptionReceived 	= false;
bool			heartBeatEmissionReceived 	= false;
bool 			emergencyHeartBeatSent		= false;

void Master::setup() {
    ConnectionState = CONNECTION_STATE_DISCONNECTED;
    char disconnected[] = "connexion";
    Commons::screen->setText((uint8_t*) disconnected, DISPLAY_USB_LINE);
    I2cLastUnusedID = 1;
    for (int i = 0 ; i < MAX_SLAVES ; i++) {
        Commons::Commons::devicePotentiometerHasChanged[i] = true;
    }
    char text[] = "Master";
    Commons::screen->setTextDisplay((uint8_t *) text, DISPLAY_ROLE_LINE, 2, 255, 0, 0, 0, 0, 0, false);
    Commons::i2cID = 0;
    Wire.begin();
}

void Master::querySlavesStatus() {
    for (int i = 1 ; i < I2cLastUnusedID ; i++) {
        Wire.beginTransmission(i);
        Wire.write(I2C_REQUEST_STATUS_FUNCCODE);
        Wire.endTransmission();
        Wire.requestFrom(i, I2C_SEND_STATUS_SIZE);
        if (Wire.available () >= I2C_SEND_STATUS_SIZE) {
            if (Wire.read() == I2C_SEND_STATUS_FUNCCODE) {
                uint16_t new_potentiometer_value = Wire.read() << 8;
                new_potentiometer_value += Wire.read();
                if (Commons::convert1024To100(new_potentiometer_value) != Commons::convert1024To100(Commons::devicesStatus[i].potentiometerValue)) //we round up the value
                    Commons::devicePotentiometerHasChanged[i] = true;
                Commons::devicesStatus[i].potentiometerValue = new_potentiometer_value;
                Commons::devicesStatus[i].buttonMask = Wire.read();
            }
            else { //we got a wrong frame, we discard the buffer
                while (Wire.available()) {
                    Wire.read();
                }
            }
        }
    }
}

void Master::searchForNewcomers() {
    Wire.beginTransmission(I2C_DEFAULT_ID);
    Wire.write(I2C_IDASSIGNATION_FUNCCODE);
    Wire.write(I2cLastUnusedID);
    Wire.endTransmission();
    Wire.requestFrom(I2C_DEFAULT_ID, I2C_IDACKNOWLEDGE_SIZE);
    if (Wire.available () >= I2C_IDACKNOWLEDGE_SIZE) {
        if (Wire.read() == I2C_IDACKNOWLEDGE_FUNCCODE) { // we received an acknowledge, we know a device exists
            I2cLastUnusedID++;
			char cstr[16];
			itoa(I2cLastUnusedID-1, cstr, 10);
			Commons::screen->setText((uint8_t*)cstr, DISPLAY_MODULARITY_LINE);
        }
    }
}

void sendDevicesUpdateToHost() {
    for (uint8_t i = 0; i < I2cLastUnusedID; i++) {
        struct DeviceStatus ds = Commons::devicesStatus[i];
        uint8_t high = ds.potentiometerValue >> 8;
        uint8_t low = ds.potentiometerValue & 0xff;
        uint8_t pot_val[4] = {HW_POTENTIOMETER_FUNCCODE, i, high, low};
        if (Commons::devicePotentiometerHasChanged[i]) {
            Commons::devicePotentiometerHasChanged[i] = false;
            Serial.write(pot_val, 4);
        }
        if (ds.buttonMask & 0b00000001) { //left button
            uint8_t switch_focus[3] = {HW_SWITCH_FOCUS_FUNCCODE, i, HW_SWITCH_FOCUS_BACKWARD};
            Serial.write(switch_focus, 3);
        }

        if (ds.buttonMask & 0b00000010) { //right button
            uint8_t switch_focus[3] = {HW_SWITCH_FOCUS_FUNCCODE, i, HW_SWITCH_FOCUS_FORWARD};
            Serial.write(switch_focus, 3);
        }
    }
}

void listenToHostForData() {
    if (Serial.available() > 0) {
        uint8_t funcCode = Serial.read();
        if (funcCode == SW_SESSION_NAME_FUNCODE) {
            uint8_t device = Serial.read();
            uint8_t buffer[SW_SESSION_NAME_SIZEVAL];
            if (device == 0) {
				int num = Serial.readBytes(reinterpret_cast<char *>(buffer), SW_SESSION_NAME_SIZEVAL - 1);
                buffer[I2C_SESSION_NAME_SIZE-1] = 0;
                Commons::screen->setText((uint8_t*)buffer, DISPLAY_SESSION_NAME_LINE);
            }
            else if (device < I2cLastUnusedID) {
				buffer[0] = I2C_SESSION_NAME_FUNCCODE;
				int num = Serial.readBytes(reinterpret_cast<char *>(&(buffer[1])), SW_SESSION_NAME_SIZEVAL - 1);
				buffer[I2C_SESSION_NAME_SIZE-1] = 0;

                Wire.beginTransmission(device);
                Wire.write(buffer, SW_SESSION_NAME_SIZEVAL);
                Wire.endTransmission();
            }
            else { //unusable data, we discard it
                while (Serial.available())
                    Serial.read();
            }
        }
        else if (funcCode == SW_PING_TO_DEVICE_FUNCCODE) {
            Serial.write(HW_PONG_TO_HOST_FUNCCODE);
			heartBeatReceptionReceived = true;
		}
		else if (funcCode == SW_PONG_TO_DEVICE_FUNCCODE) {
			heartBeatEmissionReceived = true;
		}
    }
}

void pingHostAndListenForPong() {
    Serial.write(HW_PING_TO_HOST_FUNCCODE);
    if (Serial.available() > 0) {
        uint8_t input = Serial.read();
        if (input == SW_PONG_TO_DEVICE_FUNCCODE) {
            ConnectionState = CONNECTION_STATE_CONNECTED;
			heartBeatReceptionReceived = true;
            char connected[] = "connecte";
            Commons::screen->setText((uint8_t*) connected, DISPLAY_USB_LINE);

            for (uint8_t i = 0; i < I2cLastUnusedID; i++) {
                Commons::devicePotentiometerHasChanged[i] = true;
            }
        }
    }
}

bool isHostStillAlive() {
	if (millis() - heartBeatReceptionDate >= HEARTBEAT_RECEPTION_PERIOD_MS) {
		if (heartBeatReceptionReceived) {

			heartBeatEmissionDate = millis();
			heartBeatReceptionReceived = false;
			return true;
		}
		else {
			if (!emergencyHeartBeatSent) {
				emergencyHeartBeatSent = true;
				Serial.write(HW_PING_TO_HOST_FUNCCODE);
				heartBeatEmissionDate = millis();
				heartBeatEmissionReceived = false;
			}
			else if (millis() - heartBeatEmissionDate > HEARTBEAT_EMISSION_PERIOD_MS) {
				if (heartBeatEmissionReceived) {
					emergencyHeartBeatSent = false;
					return true;
				}
				return false;
			}
			return true; //we wait a certain time before assuming host is dead
		}
	}
	return true;
}

void Master::loop() {
    Commons::screen->drawText(); //TODO would put in a common thing with "dedicated timer" maybe

    if (ConnectionState == CONNECTION_STATE_DISCONNECTED) {
        if (Commons::hasTimeElapsed(&id_assignment_timer, 10)) {
            searchForNewcomers();
            querySlavesStatus();
        }

        if (Commons::hasTimeElapsed(&ping_timer, 100)) {
			pingHostAndListenForPong();
        }
    }
    else if (ConnectionState == CONNECTION_STATE_CONNECTED) {
        if (Commons::hasTimeElapsed(&slave_status_query_timer, 10)) {
            sendDevicesUpdateToHost();
            searchForNewcomers();
            querySlavesStatus();
            Commons::updateInputs();
            listenToHostForData();
			if (isHostStillAlive()) {

			}
        }
    }

    Commons::sendSting();
}