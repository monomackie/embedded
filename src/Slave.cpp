#include "Slave.h"

#include <Wire.h>
#include <Arduino.h>
#include "i2cProtocol.h"
#include "Commons.h"
#include "Pinout.h"

volatile uint8_t    i2cData[30];                 //i2c data buffer
volatile bool       i2cReceived     = false;    //true when data has been received
volatile bool       mustNotReply    = false;
volatile bool       canSendSting    = false;

struct Timer i2c_processing_timer = {0, 0};

void Slave::setup() {
    char slavex[] = "Slave xx";
    Commons::screen->setTextDisplay((uint8_t*)slavex, DISPLAY_ROLE_LINE, 2, 0, 255, 255, 0, 0, 0, false);
    Commons::screen->drawText();
    Commons::i2cID = I2C_DEFAULT_ID;
    while (digitalRead(MSTING_INPUT_PIN) == LOW) {
        delay(10);
    }
    Wire.begin(Commons::i2cID);
    Wire.onRequest(requestI2CEvent);
    Wire.onReceive(receiveI2CEvent);
}

void Slave::receiveI2CEvent(int howMany) {
    i2cReceived = true;
    uint8_t i = 0;
    while (Wire.available()) {
        i2cData[i] = Wire.read();
        i++;

        if (mustNotReply && i2cData[i] == I2C_IDASSIGNATION_FUNCCODE) { //in case we receive an obsolete ID assignation
            while (Wire.available()) {
                Wire.read();
            }
        }
    }
}

void Slave::requestI2CEvent() {
    switch (i2cData[0]) {
        case I2C_IDASSIGNATION_FUNCCODE:
            if (!mustNotReply) {
                Wire.write(I2C_IDACKNOWLEDGE_FUNCCODE);
                mustNotReply = true;
            }
            break;
        case I2C_REQUEST_STATUS_FUNCCODE:
            uint8_t high = Commons::thisDeviceStatus.potentiometerValue >> 8;
            uint8_t low = Commons::thisDeviceStatus.potentiometerValue & 0xff;
            uint8_t buff[4] = {I2C_SEND_STATUS_FUNCCODE, high, low, Commons::thisDeviceStatus.buttonMask};
            Wire.write(buff, 4);
            Commons::thisDeviceStatus.buttonMask = 0; // &= ~(1 << i);
            canSendSting = true;
            break;
    }
}

void Slave::updateDisplayRole() {
    char roleLabel[] = "Slave x           "; //prevent stackoverflow with larger buffer
    if(Commons::i2cID != I2C_DEFAULT_ID){
        char* idPos = roleLabel + 6;
        itoa(Commons::i2cID, idPos, 10);
    }
    Commons::screen->setText(reinterpret_cast<const uint8_t *>(roleLabel), DISPLAY_ROLE_LINE);
}

void Slave::updateI2C() { //updates slave state according to what has been received with receiveI2CEvent
    if (Commons::hasTimeElapsed(&i2c_processing_timer, 2)) {
        if (i2cReceived) {  //we've got data on I2C buffer
            if (i2cData[0] == I2C_IDASSIGNATION_FUNCCODE && Commons::i2cID == I2C_DEFAULT_ID) {
                Commons::i2cID = i2cData[1];
                Wire.begin(Commons::i2cID);
                updateDisplayRole();
            }
            else if (i2cData[0] == I2C_SESSION_NAME_FUNCCODE) {

                //may need to surround this block with interrupt disable
                char text[I2C_SESSION_NAME_SIZE];
                for (uint8_t i = 0 ; i < I2C_SESSION_NAME_SIZE-1 ; i ++) {
                    text[i] = i2cData[i+1];
                }
                text[I2C_SESSION_NAME_SIZE-1] = 0;
                Commons::screen->setText((uint8_t*)text, DISPLAY_SESSION_NAME_LINE);
                Commons::screen->drawText();
            }
            i2cReceived = false;
        }
    }
}

void Slave::loop() {
    Commons::updateInputs();
    updateI2C();
    if (Commons::i2cID != I2C_DEFAULT_ID && canSendSting) {
        Commons::sendSting();
    }
}