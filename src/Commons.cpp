#include "Commons.h"
#include <Arduino.h>
#include <i2cProtocol.h>
#include "Pinout.h"

struct Timer sting_timer = {0, 0};
struct Timer input_update_timer = {0, 0};
bool stingState = false;
struct Button buttons[BUTTON_COUNT]; //[0] = left button, [1] = right button
struct DeviceStatus Commons::devicesStatus[MAX_DEVICES];
bool    Commons::devicePotentiometerHasChanged[MAX_DEVICES];
volatile struct DeviceStatus Commons::thisDeviceStatus;

Screen *Commons::screen;
uint8_t Commons::i2cID;

bool Commons::hasTimeElapsed(struct Timer *timer, unsigned long duration) {
    timer->new_time = millis();
    if (timer->new_time - timer->old_time >= duration) {
        timer->old_time = timer->new_time;
        return true;
    }
    return false;
}

void Commons::sendSting() {
    if (hasTimeElapsed(&sting_timer, I2C_WAIT_DELAY)) {
        stingState = !stingState;
        digitalWrite(MSTING_OUTPUT_PIN, stingState);
    }
}

void Commons::updateInputs() {
    if (hasTimeElapsed(&input_update_timer, 10)) {
        for (int i = 0; i < BUTTON_COUNT; i++) {
            bool previouslyPressed = buttons[i].pressed;
            bool currentlyPressed = !((bool) digitalRead(buttons[i].pin)); // ! because of pull up

            if (!previouslyPressed && currentlyPressed) { //button pushed
				buttons[i].pressed = true;
                thisDeviceStatus.buttonMask |= 1 << i;
                devicesStatus[0].buttonMask |= 1 << i;
            }
            else { //button not pushed
				buttons[i].pressed = false;
                devicesStatus[0].buttonMask &= ~(1 << i);
            }
        }
        thisDeviceStatus.potentiometerValue = analogRead(POTENTIOMETER_PIN);
        if (devicesStatus[0].potentiometerValue != roundUp1024(thisDeviceStatus.potentiometerValue)) {
            devicePotentiometerHasChanged[0] = true;
            devicesStatus[0].potentiometerValue = roundUp1024(thisDeviceStatus.potentiometerValue);
            thisDeviceStatus.potentiometerValue = devicesStatus[0].potentiometerValue;
        }
        Commons::screen->drawText(); //TODO move this to a dedicated timer
    }
}

void Commons::setupHW() {
    Serial.begin(115200);

    buttons[0] = {LEFT_BUTTON_PIN, false};
    buttons[1] = {RIGHT_BUTTON_PIN, false};
    for (uint8_t i = 0; i < BUTTON_COUNT; i++) {
        pinMode(buttons[i].pin, INPUT_PULLUP);  //button is pulled high so default value is 1
    }

    pinMode(MSTING_INPUT_PIN, INPUT_PULLUP);
    pinMode(MSTING_OUTPUT_PIN, OUTPUT);

    digitalWrite(MSTING_OUTPUT_PIN, LOW);
    delay(I2C_WAIT_DELAY * 2);
    screen = new Screen();
}

bool Commons::setupOperations() {
    bool isMaster = digitalRead(MSTING_INPUT_PIN) == HIGH; //2 attempts to check if we are slave or master
    delay(I2C_WAIT_DELAY);
    isMaster &= digitalRead(MSTING_INPUT_PIN) == HIGH;

    return isMaster;
}

uint16_t Commons::roundUp1024(int value) {
    return (int) ((float) (convert1024To100(thisDeviceStatus.potentiometerValue) * 10.23f));
}

uint8_t Commons::convert1024To100(int value) {
    float f = (float) value / 1023;
    return (uint8_t) (100 * f);
}
